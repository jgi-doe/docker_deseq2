FROM bioconductor/bioconductor_docker:RELEASE_3_15

# Update and install new Bioconductor packages from release version
RUN Rscript --vanilla -e "options(repos = c(CRAN = 'https://cran.r-project.org')); BiocManager::install(ask=FALSE)" \
    && Rscript -e 'BiocManager::install("DESeq2")'

WORKDIR /home/rstudio/

# Metadata
LABEL name="doejgi/deseq2" \
      version="3_15" \
      url="https://gitlab.com/dcassol/docker_deseq2" \
      maintainer="dcassol@lbl.gov"

