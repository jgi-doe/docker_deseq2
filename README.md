# Docker image for [Deseq2](https://bioconductor.org/packages/release/bioc/html/DESeq2.html)

## `Dockerfile`
The following is the Dockerfile for an image that contains Ubuntu and copies a script file to the docker image. 

<details>
<summary>Dockerfile</summary>
<br>

```
FROM bioconductor/bioconductor_docker:RELEASE_3_15

# Update and install new Bioconductor packages from release version
RUN Rscript --vanilla -e "options(repos = c(CRAN = 'https://cran.r-project.org')); BiocManager::install(ask=FALSE)" \
    && Rscript -e 'BiocManager::install("DESeq2")'

WORKDIR /home/rstudio/

# Metadata
LABEL name="doejgi/deseq2" \
      version="3_15" \
      url="https://gitlab.com/dcassol/docker_deseq2" \
      maintainer="dcassol@lbl.gov"
```

</details>

## Running the container

- Get a copy of public Docker image

```
docker pull doejgi/deseq2:3_15
```

- To run Rstudio Server

```
docker run -e PASSWORD=<password> \
	-p 8787:8787 \
	doejgi/deseq2:3_15
```
In the above command, `-e PASSWORD=` is setting the RStudio password and is required by the RStudio Docker image. 
It can be whatever you like except it cannot be rstudio. Log in to RStudio with the username rstudio and whatever password was specified.

- To run R from the command line:
```
docker run -it --user rstudio doejgi/deseq2:3_15 R
```
- To open a Bash shell on the container:
```
docker run -it --user rstudio doejgi/deseq2:3_15 bash
```

